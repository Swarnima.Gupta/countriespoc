import Head from 'next/head'
import styles from '../styles/Home.module.css'
import CountriesHome from '../components/Countries/countries'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>My App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <CountriesHome/>

    </div>
  )
}
