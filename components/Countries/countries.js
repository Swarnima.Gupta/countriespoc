import React, { Component } from "react"
import styles from './countries.module.css'

import "bootstrap/dist/css/bootstrap.min.css"



export default class ContriesHome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            users: [],
            searchTerm: ''
        };
        this.showDetails = this.showDetails.bind(this);
        this.searchTerm = this.search.bind(this);
    }

    componentWillMount() {
        fetch(`https://restcountries.eu/rest/v2/all`).then((res) => res.json()).then((data) => {
            this.setState({users: data})

        })
    }

    search(e) {
        console.log(e.target.value)
        this.setState({searchTerm: e.target.value})


    }

    showDetails(user) {
        user.show = !user.show;
        this.setState({...user});
        console.log(this.state)
    }

    render() {
        const list = this.state.users.map(item => ({
            id: item.id,
            header: item.name,
        }));

        return (

               <div className="container">
                   <input className="search-box" onKeyUp={(e) => this.searchTerm(e)} type="text"></input>
                   <ul className="collapse-able">
                       {this.state.users.filter(user => {
                           return user.name.toLowerCase().indexOf(this.state.searchTerm.toLowerCase()) > -1;
                       }).map((user) => {
                           return (<div className={styles.card} onClick={() => this.showDetails(user)} key={user.id}>
                                   <div className="row">
                                       <div className="col-6">
                                           <img src={user.flag} className="img-fluid"/>
                                       </div>
                                       <div className="col-6">
                                           <h2>{user.name}</h2>
                                           <span>
                                             {user.currencies && user.currencies.map((code, index) => <dd key={code.id}>
                                                 <span>{code.name}</span>
                                             </dd>)}
                                        </span>
                                           <div>
                                           <button className="btn btn-primary">Show Details</button>
                                                                                          </div>
                                       </div>
                                   </div>

                               </div>
                           );
                       })}

                   </ul>

               </div>

        );
    }
}


